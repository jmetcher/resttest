package testing;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.*;

import org.eclipse.persistence.jaxb.JAXBContextProperties;

public class TestJAXBContext {

    public static void main(String[] args) throws Exception {
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put(JAXBContextProperties.OXM_METADATA_SOURCE, "testing/planets.oxm.xml");
        JAXBContext context = JAXBContext.newInstance(new Class[] {Planets.class}, properties);

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        
        marshaller.marshal(new Planets(), System.out);
        
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        JAXBContext contextJSON = JAXBContext.newInstance(new Class[] {Planets.class}, properties);

        Marshaller marshallerJSON = contextJSON.createMarshaller();
        marshallerJSON.marshal(new Planets(), System.out);
        
    }

}