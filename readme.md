Test project for using Moxy as an XML writer in Jersey.

To start the server:
./gradlew jettyRunWar

Then load:
http://localhost:8080/resttest/rest/services/json

You should see:
{"id":1,"name":"test","radius":3.0}

which demonstrates
 1. MyMoxyContextResolver is being used to get and configure a Moxy JAXB context
 2. The JAXB context has been initialized with the planets.oxm.xml mapping file

However, if you load:
http://localhost:8080/resttest/rest/services/xml

you should see an error:
MessageBodyWriter not found for media type=application/xml, type=class testing.Planets, genericType=class testing.Planets.

which demonstrates that the mapping file is not being applied for XML.

You can also load:

http://localhost:8080/resttest/rest/services/json2

http://localhost:8080/resttest/rest/services/xml2

to show that an annotated version of the same class works just fine.
